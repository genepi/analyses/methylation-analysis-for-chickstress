## Installation of Rpackages for SeqOccin pipeline and script on https://forgemia.inra.fr/jules.sabban1/methylation-analysis-for-chickstress

rm(list=ls())

## R Packages
packages <- c("optparse",
              "ggplot2",
              "gridExtra",
              "data.table",
              "reshape2", 
              "plyr",
              "dplyr",
              "VennDiagram",
              "doMC",
              "grid", 
	      "hexbin",
	      "BiocManager",
	      "Rcpp",
	      "reshape2",
	      "RSQLite",
	      "limma",
	      "readr"
)

for(package in packages){
	install.packages(package)
}

## Bioconductor Packages
packagesBio <- c("bsseq",
		"DSS",
		"ensembldb",
              	"rtracklayer",
              	"edgeR",
              	"ade4",
              	"ChIPpeakAnno",
              	"GenomicFeatures",
              	"BSgenome.Ggallus.UCSC.galGal6",
              	"Repitools",
              	"tximport"
)

for(package in packagesBio){
	BiocManager::install(package)
}
