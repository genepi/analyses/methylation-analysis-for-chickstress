#!/usr/bin/perl 
## recupere les valeurs d'expression d'un chromosome en particulier a partir d'un fichier RNAseq
## et genere un boxplot representant la repartition des valeurs d'expression par individu et trie selon leur mediane
use Getopt::Long;

# usage : ./search_expression.pl --rna /work/project/1000rnaseqchick/internal_runs/RpRm/embr/F0_galgal5_V87dec2016_GTFrennes/Results/Summary/RSEM_multimap_summary_RpRm_embr_F0_genes_TPM.tsv --gtf list_geneID_from_RNA_file.txt /work/project/1000rnaseqchick/reference/7b_gg_EnsemblRennesAldbNcbiNoncodeFragencode_20180731_all_52075g_101188t_gg5_v87v93_withFGF21_usedForPubli_additionOfExonID_withUTRandStarStopCodons.gtf --chr Z --dir /home/jsabban/work/testBS/testExpression

GetOptions( "rna=s" => \$rna_file, 	# chemin vers RNA file
		"gtf=s" => \$gtf,	# chemin vers le GTF
		"chr=s" => \$chr, 	# chromosome recherche
		"dir=s" => \$dir 	# repertoire de travail
);

## Liste non redondante des genes_id commencant par ENSGALG dans le RNA file
unless( -e "${dir}/expression") {
   system "mkdir ${dir}/expression/";
}

system "grep ENSGALG $rna_file | awk '{print \$1}' | uniq > ${dir}/expression/list_geneID_from_RNA_file.txt";

## recupere les lignes du chromosome voulu dans le RNA file
unless( -e "${dir}/expression/expression_${chr}.txt") {
   system "touch ${dir}/expression/expression_${chr}.txt";
}
open(EX, "> ${dir}/expression/expression_${chr}.txt") or die "cannot open file : ${dir}/expression/expression_${chr}.txt";
select EX;
open(IN, "< ${dir}/expression/list_geneID_from_RNA_file.txt") or die "cannot open file : IN";

# header du fichier de sortie
print("Emb_26 \t Emb_27 \t Emb_28 \t Emb_29 \t Emb_43 \t Emb_44 \t Emb_45 \t Emb_46 \t Emb_47 \t Emb_48 \t Emb_10 \t Emb_11 \t Emb_12 \t Emb_13 \t Emb_15 \t Emb_18 \t Emb_19 \t Emb_20 \t Emb_21 \t Emb_22 \t Emb_23 \t Emb_24\n");

while(<IN>) {
	chomp;
	$gene = $_;
	$chromo = `grep $gene $gtf | head -n 1 | awk '{print \$1}'`;  # recherche dans GTF le chromosome associe au gene_id
	$chromo =~ s/[^\w]+_*//g;
	if($chromo eq $chr){
		#print("$chromo :  $gene\n");
		$trans = `grep $gene $rna_file | awk '{print \$4, \$5, \$6, \$7, \$8, \$9, \$10,\$11, \$12, \$13, \$14, \$15, \$16, \$17, \$19, \$20, \$21, \$22, \$23, \$24, \$25, \$26}'`;
		print("$trans");
	}
}
close(IN);
close EX;

system "Rscript ./expression_boxplot.R --file ${dir}/expression/expression_${chr}.txt --out ${dir}/expression --chr $chr";
