#!/usr/bin/perl 
#use try::Tiny

# prend en entree un GFF, un chromosome et un fichier contenant la liste des gènes DE et renvoie en sorti le nombre de gene DE sur ce chromosome

# use : ./count_DE_genes.pl GFF chr DE_genes

## Affichage des parametres
print("#####################\n");
print("Execution du script $0\n");
print("GFF utilise : $ARGV[0]\n");
print("Chromosome cible : $ARGV[1]\n");
print("Genes DE : $ARGV[2]\n");
print("#####################\n\n");

## Extraction de la liste des genes sur le chromosome d'interet
open(GFF, $ARGV[0]);
$chromosome=$ARGV[1];
@liste_gene=();
while(<GFF>){
	unless(/^#/){
		@ligne = split('\t', $_);
		$chr = $ligne[0];
		if($chr eq $chromosome){
			$type = $ligne[2];
			$name_find=0;
			if($type eq 'gene') {
					eval {
						@ligne=split('Name=',$_);
						@name=split(';',$ligne[1]);
						$gene=$name[0];
						$name_find=1;
					}
			}
			if($name_find){
				push(@liste_gene, $gene);
			}
		}
	}
}
close(GFF);
@liste_gene=sort(@liste_gene);

## Recherche des genes de la liste parmi les genes DE
open(DE, $ARGV[2]);
@liste_DE=();
while(<DE>){
	@ligne=split('\t', $_);
	push(@liste_DE, $ligne[0]);
}
close(DE);

### Suppression des redondances
sub unique {
	my @liste=@_;
	my %liste_unique=();
	foreach(@liste){
		$liste_unique{$_}=1;	
	}
	return(keys(%liste_unique));
}

@liste_gene=unique(@liste_gene);
@liste_DE=unique(@liste_DE);

## Verification
$nb_gene_chr=@liste_gene;
$nb_gene_DE=@liste_DE-1;
print("Il y a $nb_gene_chr genes sur le chromosome $chromosome.\n");
print("Il y a $nb_gene_DE genes DE.\n\n");


## comparaison des deux listes
%trouve=();
@commun = grep {$trouve{$_}++} @liste_gene, @liste_DE;
$gene_DE_chr=@commun;

print("Il y a $gene_DE_chr genes DE sur le chromosome $chromosome.\n");
