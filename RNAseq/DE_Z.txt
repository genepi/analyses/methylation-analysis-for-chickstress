#####################
Execution du script /home/jsabban/work/tmp/plot_soutenance/count_DE_genes.pl
GFF utilise : /work2/genphyse/genepi/RRBS/ChickStress/galgal6/Gallus_gallus.GRCg6a.99.gff3
Chromosome cible : Z
Genes DE : /home/jsabban/work/GitHub/methylation-analysis-for-chickstress/RNAseq/DE/DE_sexes_signif.txt
#####################

Il y a 575 genes sur le chromosome Z.
Il y a 245 genes DE.

Il y a 232 genes DE sur le chromosome Z.
