#!/usr/bin/perl 

# Prend en arguments un fichier contenant la liste des genes differentiellement exprimes et un fichier contenant la liste des genes differentiellement methyles
# Renvoie en sortie la liste des genes commun aux deux listes

## usage : ./search_meth_n_exp.pl DE_gene_list DM_gene_list > out
## Ex : ./search_meth_n_exp.pl /home/jsabban/work/tmp/rnaseq_gg6_0619/DE/DE_CTHS.txt /home/jsabban/work/GitHub/methylation-analysis-for-chickstress/DMC_analysis/CTHS/annotation_with_region.bed > /home/jsabban/work/GitHub/methylation-analysis-for-chickstress/DMC_analysis/CTHS/methylated_and_expressed_genes.txt

#open(DE, $ARGV[0]) or die "cannot open Differential Expression File";
$de_file=$ARGV[0];
open(DM, $ARGV[1]) or die "cannot open Differential Methylation File";

$dm=<DM>;
while(<DM>){
	chomp;
	# Collect	gene name from DM file
	@line=split("\t", $_);
	$gene=$line[9];
	($id, $gene_name)=split("_",$gene);
	
	# Search of gene_gene in DE file
	if($gene_name ne "NA"){
		$search=`grep $gene_name $de_file`;
		if(length($search) != 0){
			print("$gene_name\n");
		}
	}
}
close(DM);
